Array.prototype.isEqual = function(val){
	if(!val.join){return false;}
	if(this.length != val.length){return false;}
	var long = val.length;
	for(var i = 0; i < long; i++){
		if(val[i].join && this[i].join){
			if(!this[i].isEqual(val[i])){
				return false;
			}
		}
		else if(this[i] != val[i]){
			return false;
		}
	}
	return true;
}

Array.prototype.containsNear = function(val){
	if(!val.join){return false;}
	var long = val.length;
	for (var j = 0; j < this.length; j++) {
		if(arrondi(this[j][0], 1e6) === arrondi(val[0], 1e6)
		&& arrondi(this[j][1], 1e6) === arrondi(val[1], 1e6)
		&& arrondi(this[j][2], 1e6) === arrondi(val[2], 1e6)){
			return j;
		}
		else if(this[j] === val){
			return j;
		}
	}
	return -1;
}



function arrondi(val, decimals) {
	return Math.floor(val * decimals) / decimals;
}